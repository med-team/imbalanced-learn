imbalanced-learn (0.12.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream version. Closes: #1079676
  * Standards-Version: 4.6.2 (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Thu, 17 Oct 2024 14:09:14 +0200

imbalanced-learn (0.12.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * Remove python3-nose dependency
  * Remove old patches

 -- Alexandre Detiste <tchet@debian.org>  Sat, 25 May 2024 13:31:27 +0200

imbalanced-learn (0.12.0-2) unstable; urgency=medium

  * Team upload.
  * d/control: bump binimum sklearn version to match the changes to the
    tests.
  * d/tests/control: trim out redundant dependencies.
  * forward distutils patch upstream.

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 27 Feb 2024 15:49:01 +0100

imbalanced-learn (0.12.0-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Team upload.
  * New upstream version
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)

  [ Michael R. Crusoe ]
  * Add patch to adapt error messages. Closes: #1064223
  * d/tests/python: run for all supported Python versions.

 -- Michael R. Crusoe <crusoe@debian.org>  Mon, 26 Feb 2024 12:34:42 +0100

imbalanced-learn (0.10.0-2) unstable; urgency=medium

  * Team Upload.
  * Add patch to get rid of distutils for tests (Closes: #1056411)

 -- Nilesh Patra <nilesh@debian.org>  Sun, 10 Dec 2023 11:36:47 +0530

imbalanced-learn (0.10.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * Source is Section: science, binary package Section: python

 -- Andreas Tille <tille@debian.org>  Fri, 27 Jan 2023 14:50:02 +0100

imbalanced-learn (0.9.1-2) unstable; urgency=medium

  * Team upload.
  * d/{,t/}control: declare versioned dependency on pytest 7.
    The autopkgtest suite reveals the new upstream version of imbalanced-learn
    cannot migrate before pytest 7 because the new smoke test crashes with the
    old pytest 6.

 -- Étienne Mollier <emollier@debian.org>  Mon, 04 Jul 2022 23:01:38 +0200

imbalanced-learn (0.9.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version  (Closes: #1011858)
  * Standards-Version: 4.6.1 (routine-update)
  * d/t/python3: set a dummy home directory for running tests.

 -- Étienne Mollier <emollier@debian.org>  Sun, 03 Jul 2022 19:04:07 +0200

imbalanced-learn (0.9.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Andreas Tille ]
  * Move package to Debian Med team
  * cme fix dpkg-control
  * Add salsa-ci file (routine-update)

  [ Christian Kastner ]
  * New upstream version 0.9.0. (Closes: #999515)
  * Drop obsolete patches
  * Update Build-Depends and Depends

 -- Christian Kastner <ckk@debian.org>  Sun, 06 Feb 2022 13:00:27 +0100

imbalanced-learn (0.7.0-6) unstable; urgency=medium

  * autopkgtest: Address stderr output. (Closes: #974088, #976417)

 -- Christian Kastner <ckk@debian.org>  Sat, 05 Dec 2020 14:57:22 +0100

imbalanced-learn (0.7.0-5) unstable; urgency=medium

  * autopkgtest: Mark test "import" as superficial
  * autopkgtest: Skip failing test on armhf. Triggered by a known issue with
    scikit-learn on armhf. (Closes: #971540)

 -- Christian Kastner <ckk@debian.org>  Sun, 04 Oct 2020 22:48:37 +0200

imbalanced-learn (0.7.0-4) unstable; urgency=medium

  * Add Switch-to-new-scikit-learn-parse_version.patch
    Fixes autopkgtest failure
  * Bump python3-sklearn dependency to 0.23.2

 -- Christian Kastner <ckk@debian.org>  Mon, 17 Aug 2020 15:49:34 +0200

imbalanced-learn (0.7.0-3) unstable; urgency=medium

  * Add autopkgtest for full upstream test suite
  * Use variables from /usr/share/dpkg/architecture.mk
  * Skip one flaky test for now, resolves a FTBFS

 -- Christian Kastner <ckk@debian.org>  Sun, 16 Aug 2020 20:01:16 +0200

imbalanced-learn (0.7.0-2) unstable; urgency=medium

  * Previous version accidentally contained binaries; re-upload source-only

 -- Christian Kastner <ckk@debian.org>  Sun, 16 Aug 2020 10:50:57 +0200

imbalanced-learn (0.7.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump python3-sklearn Build-Depends to 0.23

 -- Christian Kastner <ckk@debian.org>  Sat, 15 Aug 2020 10:37:12 +0200

imbalanced-learn (0.6.2-1) unstable; urgency=medium

  * New upstream version 0.6.2
  * Refresh patches
  * Drop patch FIX-modify-regex-... (included upstream)
  * Add Rules-Requires-Root: no
  * Add myself to uploaders
  * Switch from d/compat -> 12 to debhelper-compat (= 12)
  * Bump Standards-Version to 4.5.0
  * Fix d/watch

 -- Christian Kastner <ckk@debian.org>  Mon, 20 Jan 2020 21:27:00 +0100

imbalanced-learn (0.4.3-1) unstable; urgency=medium

  * New upstream version 0.4.3 (Closes: #912456)
  * d/control: Bump Depends and Build-Depends version of sklearn to >= 0.20
  * d/control: Bump standards version to 4.3.0; no changes needed
  * Bump debhelper and compat version to 12; no changes needed
  * d/rules: Disable testsuite on i386 architecture
    + See https://github.com/scikit-learn-contrib/imbalanced-learn/issues/527
  * d/patches: Drop unnecessary debian patch for python3 builds
  * d/patches: Update doctest string represention of floats patch
  * autopkgtest: Fix hashbang on import test

 -- Aggelos Avgerinos <evaggelos.avgerinos@gmail.com>  Mon, 18 Feb 2019 13:01:03 +0200

imbalanced-learn (0.3.3-1) unstable; urgency=medium

  * Initial release (Closes: #893055)
  * d/patches: Fix build to build only for Python3
  * d/patches: Fix float string representation in tests

 -- Aggelos Avgerinos <evaggelos.avgerinos@gmail.com>  Fri, 13 Jul 2018 15:45:18 +0300
